﻿using System;

class Program
{
    static void Main()
    {
        Console.Write("Введіть кількість елементів масиву: ");
        int n = int.Parse(Console.ReadLine());

        double[] arr = new double[n];
        Random random = new Random();
        for (int i = 0; i < n; i++)
        {
            arr[i] = -2.9 + random.NextDouble() * (60.3 - (-2.9));
        }

        Console.WriteLine("Початковий масив чисел:");
        PrintArray(arr);

        double minElement = arr[0];
        double maxElement = arr[0];
        int minIndex = 0;
        int maxIndex = 0;

        for (int i = 1; i < n; i++)
        {
            if (arr[i] < minElement)
            {
                minElement = arr[i];
                minIndex = i;
            }
            if (arr[i] > maxElement)
            {
                maxElement = arr[i];
                maxIndex = i;
            }
        }

        int start = Math.Min(minIndex, maxIndex) + 1;
        int end = Math.Max(minIndex, maxIndex);

        Array.Sort(arr);

        Console.WriteLine("Масив після сортування:");
        PrintArray(arr);
    }

    static void PrintArray(double[] arr)
    {
        foreach (double element in arr)
        {
            Console.Write($"{element} ");
        }
        Console.WriteLine();
    }
}
