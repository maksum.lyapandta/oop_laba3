﻿using System;

class Program
{
    static void Main()
    {
        Console.Write("Введіть кількість елементів масиву: ");
        int n = int.Parse(Console.ReadLine());

        double[] arr = new double[n];

        Random random = new Random();

        for (int i = 0; i < n; i++)
        {
            arr[i] = -2.9 + random.NextDouble() * (60.3 - (-2.9));
        }

        Console.WriteLine("Масив чисел: " + string.Join(", ", arr));

        double minElement = arr[0];
        double maxElement = arr[0];
        int minIndex = 0;
        int maxIndex = 0;

        for (int i = 1; i < n; i++)
        {
            if (arr[i] < minElement)
            {
                minElement = arr[i];
                minIndex = (int)arr[i];
            }
            if (arr[i] > maxElement)
            {
                maxElement = arr[i];
                maxIndex = (int)arr[i];
            }

        }

        int product = maxIndex * minIndex;
        Console.WriteLine($"Добуток індексів елементів між мінімальним ({minIndex}) та максимальним ({maxIndex}) елементами = {product}");
    }
}
