﻿using System;

class Program
{
    static void Main()
    {
        Console.Write("Введіть кількість рядків: ");
        int n = int.Parse(Console.ReadLine());

        Console.Write("Введіть кількість стовпців: ");
        int m = int.Parse(Console.ReadLine());


        double[,] arr = new double[n, m];
        Random random = new Random();
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                arr[i, j] = -15.62 + random.NextDouble() * (53.36 - (-15.62));
            }
        }

        Console.WriteLine("Початковий двовимірний масив чисел:");
        PrintArray(arr);

        double[] columnSums = new double[m];
        for (int j = 0; j < m; j++)
        {
            for (int i = 0; i < n; i++)
            {
                if (arr[i, j] > 0)
                {
                    columnSums[j] += Math.Abs(arr[i, j]);
                }
            }
        }

        for (int i = 0; i < m - 1; i++)
        {
            for (int j = i + 1; j < m; j++)
            {
                if (columnSums[i] < columnSums[j])
                {
                    for (int k = 0; k < n; k++)
                    {
                        double temp = arr[k, i];
                        arr[k, i] = arr[k, j];
                        arr[k, j] = temp;
                    }

                    double tempSum = columnSums[i];
                    columnSums[i] = columnSums[j];
                    columnSums[j] = tempSum;
                }
            }
        }

        Console.WriteLine("Масив після перестановки стовпців:");
        PrintArray(arr);
    }

    static void PrintArray(double[,] arr)
    {
        int n = arr.GetLength(0);
        int m = arr.GetLength(1);

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                Console.Write($"{arr[i, j]:F2}\t");
            }
            Console.WriteLine();
        }
    }
}
