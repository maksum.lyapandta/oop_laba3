﻿using System;

class Program
{
    static void Main()
    {
        Console.Write("Введіть кількість рядків: ");
        int n = int.Parse(Console.ReadLine());

        Console.Write("Введіть кількість стовпців: ");
        int m = int.Parse(Console.ReadLine());

        double[,] arr = new double[n, m];
        Random random = new Random();
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                arr[i, j] = -15.62 + random.NextDouble() * (53.36 - (-15.62));
            }
        }

        Console.WriteLine("Початковий двовимірний масив чисел:");
        PrintArray(arr);

        int rowsWithoutPositiveElements = 0;
        for (int i = 0; i < n; i++)
        {
            bool hasPositiveElement = false;
            for (int j = 0; j < m; j++)
            {
                if (arr[i, j] > 0)
                {
                    hasPositiveElement = true;
                    break;
                }
            }
            if (!hasPositiveElement)
            {
                rowsWithoutPositiveElements++;
            }
        }

        Console.WriteLine($"Кількість рядків, які не містять жодного додатного елемента: {rowsWithoutPositiveElements}");
    }

    static void PrintArray(double[,] arr)
    {
        int n = arr.GetLength(0);
        int m = arr.GetLength(1);

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                Console.Write($"{arr[i, j]:F2}\t");
            }
            Console.WriteLine();
        }
    }
}
